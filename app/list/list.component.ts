import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  name:any;
  player:any=[];

  constructor() { }

  ngOnInit(): void {
  }
  fnSubmit(){
    let val=this.name;
    if(this.player.includes(val)){
      alert('Already present');
      return;
    }
    else{
      this.player.push(val);
      this.name='';
    }
  }

}
